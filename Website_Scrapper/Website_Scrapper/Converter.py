import json

def convert_string_to_format(input_string, answer):
    converted_data = {}
    lines = input_string.split('\n')
    
    question_line = lines[0]
    question = question_line.split('. ')[1]
    converted_data['question'] = question.strip()
    
    options = lines[1:]
    option_count = 1
    for option in options:
        try:
            if option.strip() == '':
                continue
            key = f"option{option_count}"
            value = option.split(') ')[1].strip()
            converted_data[key] = value
            option_count += 1
        except IndexError:
            print("Error: Unable to extract option from:", option)
    
    converted_data['Answer'] = answer.strip()  # Add the answer to the converted data
    return converted_data
data_list=[]
# Read the JSON files
output_file_path = r'D:\Company\Website_Scrapper\Website_Scrapper\Website_Scrapper\spiders\Questions.json'
answer_file_path = r'D:\Company\Website_Scrapper\Website_Scrapper\Website_Scrapper\spiders\Answers.json'

# Read the JSON files
with open(output_file_path, 'r') as f:
    data = json.load(f)

with open(answer_file_path, 'r') as f:
    answers_data = json.load(f)
    answers = [item['Answer'] for item in answers_data]

# Process each question dictionary with its corresponding answer
for question_dict, answer in zip(data, answers):
    # Convert the question string to the desired format
    converted_data = convert_string_to_format(question_dict['question'], answer)
    # Print or handle the converted data as needed
    
    data_list.append(converted_data)
    print(json.dumps(data_list, indent=4))


with open('QuestionsWithAnswers.json', 'w') as f:
            json.dump(data_list, f, indent=4)
    
