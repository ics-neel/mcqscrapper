import json
import re
import scrapy

class CodespiderSpider(scrapy.Spider):
    name = "codespider"
    allowed_domains = ["www.javatpoint.com"]
    start_urls = ["https://www.javatpoint.com/c-language-mcq"]

    def parse(self, response):
        data = response.css('p.pq::text, div.codeblock').getall()
        
        # Initialize variables
        current_question = None
        question_data = []
        code_block = None
        
        # Iterate over the scraped data
        for item in data:
            if item.startswith('<div class="codeblock">'):
                # Store the code block temporarily
                code_block = item
            elif current_question is None:
                # Start a new question
                current_question = {'question': item.strip()}
            elif re.match(r'^\d+\)', item):  # Check if new question
                # Append the previous question with its code block if available
                if current_question:
                    if code_block:
                        current_question['code'] = code_block
                        code_block = None
                    question_data.append(current_question)
                # Start a new question
                current_question = {'question': item.strip()}
            else:
                # Add item to the current question
                current_question['question'] += ' ' + item.strip()
        
        # Append the last question with its code block if available
        if current_question:
            if code_block:
                current_question['code'] = code_block
            question_data.append(current_question)

        # Yield each question's data
        for item in question_data:
            yield item
        with open('question.json', 'w') as f:
            json.dump(question_data, f, indent=4)

        answers = response.css('div.testanswer > p::text').getall()
        answers.append("")  # Appending an empty string for the last answer

        # Initialize variables
        current_answer = None
        answer_data = []

        # Iterate through answers
        for answer_text in answers:
            if current_answer is None:
                current_answer = {'answer': answer_text.strip()}
            else:
                if re.match(r'^\([a-z]\)', answer_text.strip()):  # Check if new answer
                    # Append the previous answer and move to the next one
                    answer_data.append(current_answer)
                    current_answer = {'answer': answer_text.strip()}

        # Writing the last answer data
        if current_answer:
            answer_data.append(current_answer)
        with open('answers.json', 'w') as f:
            json.dump(answer_data, f, indent=4)
        # Yield each answer's data
        for item in answer_data:
            yield item