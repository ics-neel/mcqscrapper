import os
import scrapy
import json

class MCQSpider(scrapy.Spider):
    name = "sanfoundry"
    allowed_domains = ["www.sanfoundry.com"]
    start_urls = ["https://www.sanfoundry.com/java-questions-answers-freshers-experienced/"]

    custom_settings = {
        'RETRY_TIMES': 3000000,  # Number of retries
        'RETRY_HTTP_CODES': [403],  # Retry on 403 responses
    }
    def convert_string_to_format(self,input_string, answer):
        converted_data = {}
        lines = input_string.split('\n')
        
        question_line = lines[0]
        question = question_line.split('. ')[1]
        converted_data['question'] = question.strip()
        
        options = lines[1:]
        option_count = 1
        for option in options:
            try:
                if option.strip() == '':
                    continue
                key = f"option{option_count}"
                value = option.split(') ')[1].strip()
                converted_data[key] = value
                option_count += 1
            except IndexError:
                print("Error: Unable to extract option from:", option)
        
        converted_data['Answer'] = answer.strip()  # Add the answer to the converted data
        return converted_data
    def parse(self, response):
        status_code = response.status
        self.logger.info(f"Received response with status code: {status_code}")
        if status_code == 200:
            # Extract text content of all <p> tags
            paragraphs = response.css('p::text').getall()

            # Initialize variables to store questions and answers
            questions = []
            current_question = ''

            # Iterate over paragraphs
            for paragraph in paragraphs:
                # Check if paragraph starts with a number followed by a period
                if paragraph.strip().startswith(('1.', '2.', '3.', '4.', '5.', '6.', '7.', '8.', '9.', '10.', '11.', '12.', '13.', '14.', '15.', '16.', '17.', '18.', '19.', '20.', '21.', '22.', '23.', '24.', '25.', '26.', '27.', '28.', '29.', '30.', '31.', '32.', '33.', '34.', '35.', '36.', '37.', '38.', '39.', '40.', '41.', '42.', '43.', '44.', '45.', '46.', '47.', '48.', '49.', '50.', '51.', '52.', '53.', '54.', '55.', '56.', '57.', '58.', '59.', '60.', '61.', '62.')):
                    # If there's a current question, append it to questions list
                    if current_question:
                        questions.append(current_question.strip())
                    # Start a new question
                    current_question = paragraph.strip()
                else:
                    # Append paragraph to current question
                    current_question += '\n' + paragraph.strip()

            # Append the last question to questions list
            if current_question:
                questions.append(current_question.strip())

            # Exclude the first and last questions
            filtered_questions = questions[1:-1]

            # Store the questions in a list
            output_data = [{'question': question} for question in filtered_questions]

            # Save the data to a JSON file
            with open('Questions.json', 'w') as f:
                json.dump(output_data, f, indent=4)
            ans=response.css('div.collapseomatic_content::text ').getall()
            # print(ans)
            # Search for answers
            paragraphs = response.css('div.collapseomatic_content::text').getall()

            self.logger.info(f"Found {len(paragraphs)} paragraphs")

            # Process the paragraphs to yield the required format
            output_data1 = []
            for i, paragraph in enumerate(paragraphs[:-1]):  # Exclude the last paragraph
                if paragraph.startswith(('Answer: ', ' Answer: ')):
                    output_data1.append({'Answer': paragraph.strip()})

            # Store the data in a JSON file
            with open('Answers.json', 'w') as f:
                json.dump(output_data1, f, indent=4)
            # Yield each question
            # for question in filtered_questions:
            #     yield {
            #         'question': question
            #     }
        elif status_code == 403:
            # Retry the request if the status code is 403
            self.logger.info("Received 403 response, retrying...")
            yield scrapy.Request(response.url, callback=self.parse, dont_filter=True)
        else:
            self.logger.error(f"Received unexpected status code: {status_code}")
            # Handle other status codes if needed
        
        data_list=[]
        # Read the JSON files
        output_file_path = r'D:\Company\Website_Scrapper\Website_Scrapper\Website_Scrapper\spiders\Questions.json'
        answer_file_path = r'D:\Company\Website_Scrapper\Website_Scrapper\Website_Scrapper\spiders\Answers.json'

        # Read the JSON files
        with open(output_file_path, 'r') as f:
            data = json.load(f)

        with open(answer_file_path, 'r') as f:
            answers_data = json.load(f)
            answers = [item['Answer'] for item in answers_data]

        # Process each question dictionary with its corresponding answer
        for question_dict, answer in zip(data, answers):
            # Convert the question string to the desired format
            converted_data = self.convert_string_to_format(question_dict['question'], answer)
            # Print or handle the converted data as needed
            
            data_list.append(converted_data)
            # print(json.dumps(data_list, indent=4))


        with open('Sanfoundry.json', 'w') as f:
                    json.dump(data_list, f, indent=4)
        os.remove('Questions.json')
        os.remove('Answers.json')
