import re
from bs4 import BeautifulSoup
import scrapy
import json
import os

class JavatpointspiderSpider(scrapy.Spider):
    name = "javatpointspider"
    allowed_domains = ["www.javatpoint.com"]
    start_urls = ["https://www.javatpoint.com/c-language-mcq"]

    def parse(self, response):
        data = response.css('p.pq::text, div.codeblock').getall()
        
        # Initialize variables
        current_question = None
        question_data = []
        code_block = None
        
        # Iterate over the scraped data
        for item in data:
            if item.startswith('<div class="codeblock">'):
                # Store the code block temporarily
                code_block = item
            elif current_question is None:
                # Start a new question
                current_question = {'question': item.strip()}
            elif re.match(r'^\d+\)', item):  # Check if new question
                # Append the previous question with its code block if available
                if current_question:
                    if code_block:
                        current_question['code'] = code_block
                        code_block = None
                    question_data.append(current_question)
                # Start a new question
                current_question = {'question': item.strip()}
            else:
                # Add item to the current question
                current_question['question'] += ' ' + item.strip()
        
        # Append the last question with its code block if available
        if current_question:
            if code_block:
                current_question['code'] = code_block
            question_data.append(current_question)

        # Yield each question's data
        for item in question_data:
            yield item
        with open('question.json', 'w') as f:
            json.dump(question_data, f, indent=4)

        answers = response.css('div.testanswer > p::text').getall()
        answers.append("")  # Appending an empty string for the last answer

        # Initialize variables
        current_answer = None
        answer_data = []

        # Iterate through answers
        for answer_text in answers:
            if current_answer is None:
                current_answer = {'answer': answer_text.strip()}
            else:
                if re.match(r'^\([a-z]\)|^\[A-D]\)', answer_text.strip()):  # Check if new answer
                    # Append the previous answer and move to the next one
                    answer_data.append(current_answer)
                    current_answer = {'answer': answer_text.strip()}

        # Writing the last answer data
        if current_answer:
            answer_data.append(current_answer)
        with open('answers.json', 'w') as f:
            json.dump(answer_data, f, indent=4)
        # Yield each answer's data
        for item in answer_data:
            yield item



        #asdasldkhsdhdsdddddddddddddddddddddddddddddddddddddd
        with open('answers.json', 'r') as f:
            options_data = json.load(f)

        # Load data from combined_data.json
        with open('question.json', 'r') as f:
            combined_data = json.load(f)

        # Initialize list to store combined records
        combined_records = []

        # Iterate through each record and combine them
        for i in range(len(combined_data)):
            combined_record = combined_data[i]
            options_record = options_data[i]

            # Combine the record
            combined_record.update(options_record)

            # Append combined record to the list
            combined_records.append(combined_record)

        # Write combined records to a new JSON file
        with open('combined_dataQA.json', 'w') as f:
            json.dump(combined_records, f, indent=4)

      
        # Extract list items using XPath
        list_items = response.xpath('//ol[@class="pointsa"]/li')
        
        # Initialize variables
        output_data = []
        
        # Iterate over the list items
        for li in list_items:
            # Extract the text content of the list item
            text_content = li.xpath('string()').get().strip()
            
            # Append the text content to the output data
            output_data.append(text_content)
        
        # Group the text content into groups of four
        grouped_data = [output_data[i:i+4] for i in range(0, len(output_data), 4)]
        
        # Transform the grouped data into JSON format
        json_data = []
        for group in grouped_data:
            json_group = {}
            if len(group) >= 4:  # Check if group has at least 4 elements
                json_group = {
                    'option1': group[0],
                    'option2': group[1],
                    'option3': group[2],
                    'option4': group[3]
                }
            json_data.append(json_group)
        
        # Store the data in a JSON file
        with open('options.json', 'w') as f:
            json.dump(json_data, f, indent=4)
        
        # aklsajkjlfkalkjhldjsadsf
        with open('options.json', 'r') as f:
            options_data = json.load(f)

        # Load data from combined_data.json
        with open('combined_dataQA.json', 'r') as f:
            combined_data = json.load(f)

        # Initialize list to store combined records
        combined_records = []

        # Iterate through each record and combine them
        for i in range(len(combined_data)):
            try:
                combined_record = combined_data[i]
                options_record = options_data[i]

                # Combine the record
                combined_record.update(options_record)

                # Append combined record to the list
                combined_records.append(combined_record)
            except IndexError:
                pass

        # Write combined records to a new JSON file
        with open('com.json', 'w') as f:
            json.dump(combined_records, f, indent=4)


            
        os.remove('combined_dataQA.json')
        os.remove('options.json')
        os.remove('question.json')
        os.remove('answers.json')
      

        # # Remove temporary files
        with open('com.json', 'r') as file:
            data = json.load(file)

        # Iterate over the data
        for item in data:
            code_block = item.get('code')
            if code_block:
                # Parse the HTML content with BeautifulSoup
                soup = BeautifulSoup(code_block, 'html.parser')
                # Find the textarea element within the code block
                textarea = soup.find('textarea', {'name': 'code'})
                if textarea:
                    # Get the text content from the textarea
                    c_code = textarea.get_text().strip()
                    # Process or yield the extracted C code
                    item['c_code'] = c_code

        # Write the modified data back to the JSON file
        with open('output.json', 'w') as file:
            json.dump(data, file, indent=4)
        os.remove('com.json')
        # os.remove(json_data_path)
